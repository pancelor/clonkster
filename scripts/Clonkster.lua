local dev,dev_bard
-- dev=true
dev_bard=dev

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local character = require "necro.game.character.Character"
local collision = require "necro.game.tile.Collision"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local flyaway = require "necro.game.system.Flyaway"
local inventory = require "necro.game.item.Inventory"
local invincibility = require "necro.game.character.Invincibility"
local map = require "necro.game.object.Map"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

customEntities.extend {
  name="clonkster",
  template=customEntities.template.player(dev_bard and 9 or 0),
  components={
    {
      friendlyName={name="clonkster"},
      initialEquipment={
        items={
          "ShovelBasic",
          "Club",
          "Torch1",
          "Bomb",
        },
      },
      inventoryCursedSlots={
        slots={weapon=true},
        exemptItems={Club=true},
      },
      bestiary={image="ext/bestiary/bestiary_ogre.png"},
      playableCharacter={
        lobbyOrder=1,
      },
      clonkster_player={},
      -- https://gitlab.com/Marukyu/Synchrony/-/blob/master/scripts/necro/game/data/enemy/Z4/Ogre.lua
      -- clonkingSprite={},
    },
  },
}

components.register {
  club={
    components.constant.int8("knockbackDistance",3),
  },
  player={},
}

event.entitySchemaLoadNamedEntity.add("debug", {key="Club"}, function (ev)
  ev.entity.clonkster_club={}
  -- do less damage to monkeys:
  ev.entity.weapon.damage=1
  ev.entity.weaponReloadable.pattern.tiles[1].damageMultiplier=5
end)

-- event.turn.add("clonkingFlyaway", {order="playerActions", filter="clonkster_player"}, function (ev)
--   dbg(ev.entity)
--   character.setDynamicSprite(ev.entity, ev.entity.clonkingSprite.sprites[clonking.direction])
-- end)

event.holderKill.add("clonkingFlyaway", {order="credit", filter="clonkster_club"}, function (ev)
  if ev.damageType==damage.Type.PHYSICAL then
    flyaway.create{entity=ev.victim, text="Clonked!"}
  end
end)

event.weaponAttack.add("doClubKnockback", {order="knockback", filter="clonkster_club"}, function(ev)
  if ev.loadedAttack then
    local knockbackDir=action.rotateDirection(ev.direction, action.Rotation.MIRROR)
    move.direction(ev.attacker, knockbackDir, ev.weapon.clonkster_club.knockbackDistance)

    -- todo this is kinda janky, and looks weird.
    -- todo how to redirect minotaurs?
    local dx,dy=action.getMovementOffset(knockbackDir)
    for _, entityID in ipairs(map.getAll(ev.attacker.position.x+dx,ev.attacker.position.y+dy)) do
      local entity = ecs.getEntityByID(entityID)
      if entity:hasComponent("collision")
      and collision.Type.check(entity.collision.mask, collision.Type.ENEMY) then
        move.direction(entity,knockbackDir,1,bit.bor(move.Flag.TWEEN,move.Flag.COLLIDE_INTERMEDIATE))
      end
    end
  end
end)

-- event.entitySchemaLoadNamedEnemy.add("debug2", {key="ogre"}, dbg)
